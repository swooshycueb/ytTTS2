# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file, you
# can obtain one at http://mozilla.org/MPL/2.0/.
#
# Copyright (C) 2018 Markus Kitsinger (SwooshyCueb) <root@swooshalicio.us>

import threading, tempfile, os
from gtts import gTTS
from playsound import playsound
from collections import deque

class _tts_thread(threading.Thread):
	def __init__(self):
		self.should_stop = False
		self.__messages = deque()
		self.__playlock = threading.Condition()
		self.__gttslock = threading.Lock()
		self.__listlock = threading.Lock()
		self.__tempdir = tempfile.TemporaryDirectory(prefix="tts.")
		super().__init__(name="tts")

	def process_message(self, message):
		tfd, tfn = tempfile.mkstemp(suffix=".mp3", dir=self.__tempdir.name)
		with self.__gttslock:
			tts = gTTS(message, lang='en-uk')
			with os.fdopen(tfd, mode='wb') as tfp:
				tts.write_to_fp(tfp)
		with self.__listlock:
			self.__messages.append(tfn)
		with self.__playlock:
			self.__playlock.notify()

	def run(self):
		while not self.should_stop:
			while len(self.__messages) > 0:
				tfn = None
				with self.__listlock:
					tfn = self.__messages.popleft()
				playsound(tfn)
				os.remove(tfn)
			with self.__playlock:
				self.__playlock.wait(timeout=5)

	def stop(self, join=True):
		self.should_stop = True
		with self.__playlock:
			self.__playlock.notify()
		if join:
			self.join()

tts_thread = _tts_thread()
