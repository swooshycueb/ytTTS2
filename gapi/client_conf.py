# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file, you
# can obtain one at http://mozilla.org/MPL/2.0/.
#
# Copyright (C) 2018 Markus Kitsinger (SwooshyCueb) <root@swooshalicio.us>

project_id = "tts-for-narci"
client_id = "330674672253-paqtrfglp0k8o37o5aok9a54d1eu3fbe.apps.googleusercontent.com"
client_secret = "2gsQhSse7_esT5DoBEvp0RwY"
auth_uri = "https://accounts.google.com/o/oauth2/auth"
token_uri = "https://accounts.google.com/o/oauth2/token"
auth_provider_x509_cert_url = "https://www.googleapis.com/oauth2/v1/certs"

client_config = {
	"installed": {
		"project_id": project_id,
		"client_id": client_id,
		"client_secret": client_secret,
		"auth_uri": auth_uri,
		"token_uri": token_uri,
		"auth_provider_x509_cert_url": auth_provider_x509_cert_url,
		"redirect_uris": [
			"urn:ietf:wg:oauth:2.0:oob",
			"http://localhost"
		]
	}
}

scopes = [
	"https://www.googleapis.com/auth/youtube.readonly"
]
