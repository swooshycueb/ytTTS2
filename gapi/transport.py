# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file, you
# can obtain one at http://mozilla.org/MPL/2.0/.
#
# Copyright (C) 2018 Markus Kitsinger (SwooshyCueb) <root@swooshalicio.us>

from google.auth import transport
from google.auth.transport.requests import AuthorizedSession, _Response

class AuthorizedHttp_Requests(object):
	def __init__(self, credentials, http=None, **kwargs):
		self.session = AuthorizedSession(credentials, **kwargs)

	def request(self, uri, method='GET', body=None, headers=None, **kwargs):
		response = self.session.request(method, uri, data=body, headers=headers, **kwargs)
		return _Response(response), response.text
