# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file, you
# can obtain one at http://mozilla.org/MPL/2.0/.
#
# Copyright (C) 2018 Markus Kitsinger (SwooshyCueb) <root@swooshalicio.us>

from .serialization import serializables
from camel import Camel
from google.oauth2.credentials import Credentials
from . import client_conf
import io, os

class credential_cache(object):
	def __init__(self, access_token, refresh_token):
		self.access_token = access_token
		self.refresh_token = refresh_token

	@staticmethod
	def save(creds, path):
		with io.open(path, mode="w", encoding="utf-8") as creds_io:
			creds_io.write(Camel([serializables]).dump(credential_cache(creds.token, creds.refresh_token)))

	@staticmethod
	def load(path):
		if os.path.exists(path):
			if os.path.getsize(path) > 10000:
				# file too big
				# TODO: throw exception
				os.remove(path)

		if not os.path.exists(path):
			return None

		creds_str = None
		with io.open(path, mode="r", encoding="utf-8") as creds_io:
			creds_str = creds_io.read()

		creds = Camel([serializables]).load(creds_str)

		if creds == None:
			return None

		c = Credentials(
			creds.access_token,
			refresh_token=creds.refresh_token,
			token_uri=client_conf.token_uri,
			client_id=client_conf.client_id,
			client_secret=client_conf.client_secret
		)

		return c


@serializables.dumper(credential_cache, 'credentials', version=1)
def __dumper_v1(creds):
	return dict(
		access_token = creds.access_token,
		refresh_token = creds.refresh_token
	)

@serializables.loader('credentials', version=1)
def __loader_v1(data, version):
	return credential_cache(data["access_token"], data["refresh_token"])
