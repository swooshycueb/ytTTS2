# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file, you
# can obtain one at http://mozilla.org/MPL/2.0/.
#
# Copyright (C) 2018 Markus Kitsinger (SwooshyCueb) <root@swooshalicio.us>

from .client_conf import *
from .credential_cache import *
from camel import Camel
from google_auth_oauthlib.flow import InstalledAppFlow
from google.oauth2.credentials import Credentials
from google.auth.transport.requests import Request
import os, random

def get_credentials(cache_dir):
	creds_file = os.path.join(cache_dir, "credentials.camel")

	if not os.path.exists(cache_dir):
		os.makedirs(cache_dir)

	creds = credential_cache.load(creds_file)

	if creds is None:
		flow = InstalledAppFlow.from_client_config(client_config, scopes)
		ports = list(range(8000, 10000))
		random.shuffle(ports)
		for p in ports:
			try:
				creds = flow.run_local_server(port=p)
			except OSError as err:
				if err.errno != 98:
					raise
			except:
				raise
			else:
				break
		if len(creds.token) < 20 and len(creds.refresh_token) < 20:
			return None
	else:
		creds.refresh(Request())

	credential_cache.save(creds, creds_file)

	return creds
