# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file, you
# can obtain one at http://mozilla.org/MPL/2.0/.
#
# Copyright (C) 2018 Markus Kitsinger (SwooshyCueb) <root@swooshalicio.us>

from .. import transport
import apiclient
from apiclient.discovery import build

class service(object):
	def __init__(self, creds):
		self.creds = creds
		self.http = transport.AuthorizedHttp_Requests(creds)
		self.service = build("youtube", "v3", http=self.http, requestBuilder=self.__build_request)

	def get_owned_broadcasts(self, page=None, raw=False, maxPages=20):
		if page != None:
			r = self.service.liveBroadcasts().list(part="id,snippet,status", broadcastType="all", mine=True, pageToken=page)
			q = r.execute()
			if raw:
				return q
			if 'items' in q:
				return q['items']
			return []

		r = self.service.liveBroadcasts().list(part="id,snippet,status", broadcastType="all", mine=True)
		q = r.execute()
		if raw:
			return q
		if 'items' in q:
			ret = q['items']
		else:
			ret = []

		q2 = q
		for x in range(0, maxPages):
			if 'nextPageToken' in q2:
				q2 = self.get_owned_broadcasts(page=q2['nextPageToken'], raw=True)
				if 'items' in q2:
					ret = ret + q2['items']
			else:
				break

		return ret

	def get_active_livechats(self, maxPages=20):
		ret = dict()
		bcasts = self.get_owned_broadcasts()
		for bcast in bcasts:
			if 'snippet' in bcast:
				if 'liveChatId' in bcast['snippet']:
					if bcast['snippet']['liveChatId'] not in ret:
						lchat = {
							"broadcastId": bcast['id'],
							"broadcastTitle": bcast['snippet']['title'],
							"channelId": bcast['snippet']['channelId']
						}
						ret[bcast['snippet']['liveChatId']] = lchat
		return ret

	def get_livechat_messages(self, liveChatId, maxResults=2000, page=None, raw=False):
		if page != None:
			r = self.service.liveChatMessages().list(liveChatId=liveChatId, part="id,snippet,authorDetails", maxResults=maxResults, pageToken=page)
		else:
			r = self.service.liveChatMessages().list(liveChatId=liveChatId, part="id,snippet,authorDetails", maxResults=maxResults)
		q = r.execute()

		if raw:
			return q

		if 'items' in q:
			ret1 = q['items']
		else:
			ret1 = []

		if 'nextPageToken' in q:
			ret2 = q['nextPageToken']
		else:
			ret2 = None

		if 'pollingIntervalMillis' in q:
			ret3 = q['pollingIntervalMillis']
		else:
			ret3 = 0

		return ret1, ret2, ret3

	def __build_request(self, http, *args, **kwargs):
		return apiclient.http.HttpRequest(self.http, *args, **kwargs)



