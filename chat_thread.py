# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file, you
# can obtain one at http://mozilla.org/MPL/2.0/.
#
# Copyright (C) 2018 Markus Kitsinger (SwooshyCueb) <root@swooshalicio.us>

from datetime import datetime, timedelta
from tts_thread import tts_thread
import threading

class chat_thread(threading.Thread):
	def __init__(self, service, livechatID):
		self.should_stop = False
		self.service = service
		self.livechatID = livechatID
		super().__init__(name=livechatID)

	def run(self):
		page = None
		while True:
			if self.should_stop:
				break
			msgs, page, msec_wait = self.service.get_livechat_messages(self.livechatID, page=page)
			currcallstamp = datetime.utcnow()
			nextcallstamp = currcallstamp + timedelta(milliseconds=msec_wait)
			chat_ended = False
			for msg in msgs:
				msg_text = None
				if 'snippet' in msg:
					if 'type' in msg['snippet']:
						if msg['snippet']['type'] == 'chatEndedEvent':
							chat_ended = True
				if 'authorDetails' in msg:
					if 'isChatOwner' in msg['authorDetails']:
						if msg['authorDetails']['isChatOwner']:
							if 'snippet' in msg:
								if 'textMessageDetails' in msg['snippet'] and 'messageText' in msg['snippet']['textMessageDetails']:
									msg_text = msg['snippet']['textMessageDetails']['messageText']
								elif 'displayMessage' in msg['snippet']:
									msg_text = msg['snippet']['displayMessage']
								elif 'superChatDetails' in msg['snippet'] and 'userComment' in msg['snippet']['superChatDetails']:
									msg_text = msg['snippet']['superChatDetails']['userComment']
							if msg_text != None:
								print(msg_text)
								if page != None:
									tts_thread.process_message(msg_text)
			if chat_ended:
				break
			while (nextcallstamp >= datetime.utcnow()) and not self.should_stop:
				pass

	def stop(self, join=True):
		self.should_stop = True
		if join:
			self.join()
