# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file, you
# can obtain one at http://mozilla.org/MPL/2.0/.
#
# Copyright (C) 2018 Markus Kitsinger (SwooshyCueb) <root@swooshalicio.us>

from apiclient.discovery import build
from google.oauth2.credentials import Credentials
from pprint import pprint
import appdirs, apiclient, os, io, httplib2
import gapi.auth
import gapi.transport
from gapi.yt import service
from chat_thread import chat_thread
from tts_thread import tts_thread

product_name = "ytTTS2"

config_dir = os.path.join(appdirs.user_config_dir(), product_name)
cache_dir = os.path.join(appdirs.user_cache_dir(), product_name)

if __name__ == "__main__":
	tts_thread.start()
	creds = gapi.auth.get_credentials(cache_dir)
	yt = service.service(creds)
	chats = yt.get_active_livechats()
	for id, deets in chats.items():
		cthread = chat_thread(yt, id)
		cthread.start()
		deets["thread"] = cthread


